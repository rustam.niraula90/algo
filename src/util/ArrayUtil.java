package util;

import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class ArrayUtil {
    public static int[] createUnsortedArray(int size) {
        return shuffle(createArray(size));
    }

    public static int[] createSortedArray(int size) {
        return createArray(size);
    }

    public static int[] createRandomArray(int size, int lowerBound, int upperBound) {
        int[] arr = new int[size];
        Random random = new Random();
        for (int i = 0; i < size; i++) {
            arr[i] = random.nextInt(lowerBound, upperBound);
        }
        return arr;
    }

    public static int[] shuffle(int[] arr) {
        List<Integer> list = IntStream.of(arr).boxed().collect(Collectors.toList());
        Collections.shuffle(list);
        return list.stream().mapToInt(i -> i).toArray();
    }

    public static int[] createArray(int size) {
        int[] arr = new int[size];
        for (int i = 0; i < size; i++)
            arr[i] = i;
        return arr;
    }
}
