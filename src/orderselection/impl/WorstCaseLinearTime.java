package orderselection.impl;

import orderselection.Selection;

public class WorstCaseLinearTime extends Selection {
    @Override
    public int select(int[] arr, int i) {
        return 0;
    }

    @Override
    public String getAlgorithm() {
        return "WORST CASE LINEAR TIME SELECTION";
    }
}
