package orderselection.impl;

import orderselection.Selection;

import java.util.Random;

public class ExpectedLinearTime extends Selection {
    @Override
    public int select(int[] arr, int i) {
        return quickSelect(arr, 0, arr.length - 1, i);
    }

    private int quickSelect(int[] arr, int l, int h, int i) {
        if (l == h) return arr[l];
        int p = randomPartition(arr, l, h);
        int k = p - l + 1;
        if (i <= k)
            return quickSelect(arr, l, p, i);
        else
            return quickSelect(arr, p + 1, h, i - k);
    }

    private int randomPartition(int[] arr, int l, int h) {
        int random = new Random().nextInt(l, h + 1);
        swap(arr, random, h);
        return partition(arr, l, h);
    }

    private int partition(int[] arr, int l, int h) {
        int pivot = arr[h];
        int x = l;
        for (int i = l; i < h; i++)
            if (arr[i] < pivot) {
                swap(arr, i, x);
                x++;
            }
        swap(arr, x, h);
        return x;
    }

    @Override
    public String getAlgorithm() {
        return "EXPECTED LINEAR TIME SELECTION";
    }
}
