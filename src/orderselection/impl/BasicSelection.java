package orderselection.impl;

import orderselection.Selection;

import java.util.Arrays;

public class BasicSelection extends Selection {
    @Override
    public int select(int[] arr, int i) {
        Arrays.sort(arr);
        return arr[i - 1];
    }

    @Override
    public String getAlgorithm() {
        return "BASIC SELECTION";
    }
}
