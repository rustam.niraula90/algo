package orderselection;

public abstract class Selection {

    public abstract int select(int arr[], int i);

    public abstract String getAlgorithm();

    protected void swap(int[] arr, int index1, int index2) {
        int t = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = t;
    }

}
