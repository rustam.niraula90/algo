package orderselection;

import orderselection.impl.BasicSelection;
import orderselection.impl.ExpectedLinearTime;
import orderselection.impl.WorstCaseLinearTime;
import util.ArrayUtil;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        final int array_size = 1000000;
        int[] arr = ArrayUtil.createUnsortedArray(array_size);
        int i = new Random().nextInt(1, array_size + 1);
        select(arr, i, new BasicSelection());
        select(arr, i, new ExpectedLinearTime());
        select(arr, i, new WorstCaseLinearTime());
    }

    private static int select(int[] arr, int i, Selection selection) {
        System.out.println(selection.getAlgorithm());
        long start = System.currentTimeMillis();
        int elem = selection.select(arr, i);
        long end = System.currentTimeMillis();
        System.out.println("order: " + i + " element: " + elem);
        System.out.println("Runtime: " + (end - start) + " ms\n");
        return elem;
    }
}
