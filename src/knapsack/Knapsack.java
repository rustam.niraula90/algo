package knapsack;

public abstract class Knapsack {

    public abstract double calculate(int n, int w[], int v[], int W);

    public abstract String getAlgorithm();
}
