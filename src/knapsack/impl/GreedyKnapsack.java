package knapsack.impl;

import knapsack.Knapsack;

import java.util.Arrays;
import java.util.Collections;

public class GreedyKnapsack extends Knapsack {
    @Override
    public double calculate(int n, int[] w, int[] v, int W) {
        order(n, w, v);
        double profit = 0.0;
        int i = 0;
        while (i < n) {
            if (w[i] > W) break;
            profit += v[i];
            W -= w[i];
            i++;
        }
        if (i < n && W > 0) {
            profit += v[i] * (W / (double) w[i]);
        }
        return profit;
    }

    private void order(int n, int[] w, int[] v) {
        Pair[] pairs = new Pair[n];
        for (int i = 0; i < n; i++) {
            pairs[i] = new Pair(w[i], v[i]);
        }
        Arrays.sort(pairs, Collections.reverseOrder());
        for (int i = 0; i < n; i++) {
            w[i] = pairs[i].w;
            v[i] = pairs[i].v;
        }
    }

    static class Pair implements Comparable<Pair> {
        int w;
        int v;

        public Pair(int w, int v) {
            this.w = w;
            this.v = v;
        }

        @Override
        public int compareTo(Pair other) {
            double f1 = this.v / (double) this.w;
            double f2 = other.v / (double) other.w;
            return (int) Math.round(f1 - f2);
        }
    }

    @Override
    public String getAlgorithm() {
        return "GREEDY KNAPSACK";
    }
}
