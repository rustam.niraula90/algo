package knapsack.impl;

import knapsack.Knapsack;

public class BacktrackKnapsack extends Knapsack {
    @Override
    public double calculate(int n, int[] w, int[] v, int W) {
        return backtrack(n - 1, w, v, W, 0);
    }

    public int backtrack(int n, int[] w, int[] v, int W, int profit) {
        if (n == -1 || W == 0) return profit;
        int p1 = 0;
        if (W - w[n] >= 0)
            p1 = backtrack(n - 1, w, v, W - w[n], profit + v[n]);
        int p2 = backtrack(n - 1, w, v, W, profit);
        return Math.max(p1, p2);
    }

    @Override
    public String getAlgorithm() {
        return "BACK-TRACK KNAPSACK";
    }
}
