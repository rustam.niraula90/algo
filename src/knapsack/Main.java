package knapsack;

import knapsack.impl.BacktrackKnapsack;
import knapsack.impl.DynamicKnapsack;
import knapsack.impl.GreedyKnapsack;
import util.ArrayUtil;

import java.util.Arrays;
import java.util.Random;

public class Main {
    public static void main(String[] args) {
        final int size = 20;
        final int upperBound = 100;
        int W = new Random().nextInt(1, upperBound);
        int[] w = ArrayUtil.createRandomArray(size, 1, upperBound);
        int[] v = ArrayUtil.createRandomArray(size, 1, upperBound);
        System.out.println("v: " + Arrays.toString(v));
        System.out.println("w: " + Arrays.toString(w));
        System.out.println("W: " + W + "\n");
        calculate(w, v, W, new GreedyKnapsack());
        calculate(w, v, W, new BacktrackKnapsack());
        calculate(w, v, W, new DynamicKnapsack());
    }

    private static double calculate(int[] w, int[] v, int W, Knapsack knapsack) {
        System.out.println(knapsack.getAlgorithm());
        long start = System.currentTimeMillis();
        double profit = knapsack.calculate(w.length, w, v, W);
        long end = System.currentTimeMillis();
        System.out.println("max profit: " + profit);
        System.out.println("Runtime: " + (end - start) + " ms\n");
        return profit;
    }
}
