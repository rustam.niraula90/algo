package gcd;

import gcd.Gcd;
import gcd.impl.EuclidGcd;
import gcd.impl.ExtendedEuclidGcd;
import gcd.impl.IterativeGcd;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        final int bound = 100000;
        Random random = new Random();
        int a = random.nextInt(bound);
        int b = random.nextInt(bound);
        gcd(a, b, new IterativeGcd());
        gcd(a, b, new EuclidGcd());
        gcd(a, b, new ExtendedEuclidGcd());
    }

    private static int gcd(int a, int b, Gcd gcd) {
        System.out.println(gcd.getAlgorithm());
        long start = System.currentTimeMillis();
        int output = gcd.findGcd(a, b);
        long end = System.currentTimeMillis();
        System.out.printf("GCD of %d and %d is %d\n", a, b, output);
        System.out.println("Runtime: " + (end - start) + " ms\n");
        return output;
    }
}
