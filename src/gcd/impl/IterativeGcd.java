package gcd.impl;

import gcd.Gcd;

public class IterativeGcd extends Gcd {
    @Override
    public int findGcd(int a, int b) {
        if (a == 0) return b;
        else if (b == 0) return a;
        else {
            while (b != 0) {
                int r = a % b;
                a = b;
                b = r;
            }
            return a;
        }
    }

    @Override
    public String getAlgorithm() {
        return "ITERATIVE GCD";
    }
}
