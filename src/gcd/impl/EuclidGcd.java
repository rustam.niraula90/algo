package gcd.impl;

import gcd.Gcd;

public class EuclidGcd extends Gcd {
    @Override
    public int findGcd(int a, int b) {
        if (a == 0) return b;
        return findGcd(b % a, a);
    }

    @Override
    public String getAlgorithm() {
        return "EUCLID'S GCD";
    }
}
