package gcd.impl;

import gcd.Gcd;

public class ExtendedEuclidGcd extends Gcd {
    @Override
    public int findGcd(int a, int b) {
        GcdHolder xy = getGcd(a, b);
        System.out.printf("%d * (%d) + %d * (%d) = gcd(%d,%d)\n", a, xy.x, b, xy.y, a, b);
        return xy.gcd;
    }

    private GcdHolder getGcd(int a, int b) {
        if (a == 0)
            return new GcdHolder(b, 0, 1);
        GcdHolder xy1 = getGcd(b % a, a);
        int x = xy1.y - (b / a) * xy1.x;
        return new GcdHolder(xy1.gcd, x, xy1.x);

    }

    static class GcdHolder {
        int gcd;
        int x;
        int y;

        public GcdHolder(int gcd, int x, int y) {
            this.gcd = gcd;
            this.x = x;
            this.y = y;
        }
    }

    @Override
    public String getAlgorithm() {
        return "EXTENDED EUCLID'S GCD";
    }
}
