package gcd;

public abstract class Gcd {

    public abstract int findGcd(int a, int b);

    public abstract String getAlgorithm();
}
