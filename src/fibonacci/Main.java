package fibonacci;

import fibonacci.impl.IterativeFibonacci;
import fibonacci.impl.RecursiveFibonacci;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        final int bound = 47;
        Random random = new Random();
        int a = random.nextInt(bound);
        gcd(a, new IterativeFibonacci());
        gcd(a, new RecursiveFibonacci());
    }

    private static int gcd(int a, Fibonacci gcd) {
        System.out.println(gcd.getAlgorithm());
        long start = System.currentTimeMillis();
        int output = gcd.findFibonacci(a);
        long end = System.currentTimeMillis();
        System.out.printf("%d of fibonacci number is %d\n", a, output);
        System.out.println("Runtime: " + (end - start) + " ms\n");
        return output;
    }
}
