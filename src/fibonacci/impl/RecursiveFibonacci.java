package fibonacci.impl;

import fibonacci.Fibonacci;

public class RecursiveFibonacci extends Fibonacci {
    @Override
    public int findFibonacci(int x) {
        if (x == 0) return 0;
        else if (x == 1) return 1;
        return findFibonacci(x - 1) + findFibonacci(x - 2);
    }

    @Override
    public String getAlgorithm() {
        return "RECURSIVE FIBONACCI";
    }
}
