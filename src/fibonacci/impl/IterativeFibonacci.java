package fibonacci.impl;

import fibonacci.Fibonacci;

public class IterativeFibonacci extends Fibonacci {
    @Override
    public int findFibonacci(int x) {
        if (x == 0) return 0;
        else {
            int n1 = 0, n2 = 1, n;
            for (int i = 1; i < x; i++) {
                n = n1 + n2;
                n1 = n2;
                n2 = n;
            }
            return n2;
        }
    }

    @Override
    public String getAlgorithm() {
        return "ITERATIVE FIBONACCI";
    }
}
