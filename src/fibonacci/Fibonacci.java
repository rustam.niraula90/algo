package fibonacci;

public abstract class Fibonacci {

    public abstract int findFibonacci(int x);

    public abstract String getAlgorithm();
}
