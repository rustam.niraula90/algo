package sort;

import sort.impl.*;

import java.util.Arrays;

import static util.ArrayUtil.createUnsortedArray;

public class Main {

    public static void main(String[] args) {
        int[] arr = createUnsortedArray(10000);
        System.out.println("Array: " + Arrays.toString(arr));
        sort(Arrays.copyOf(arr, arr.length), new BubbleSort());
        sort(Arrays.copyOf(arr, arr.length), new SelectionSort());
        sort(Arrays.copyOf(arr, arr.length), new InsertionSort());
        sort(Arrays.copyOf(arr, arr.length), new MergeSort());
        sort(Arrays.copyOf(arr, arr.length), new QuickSort());
        sort(Arrays.copyOf(arr, arr.length), new RandomizedQuickSort());
        sort(Arrays.copyOf(arr, arr.length), new HeapSort());

    }

    private static void sort(int[] arr, Sort sort) {
        System.out.println(sort.getAlgorithm());
        System.out.println("Before: " + Arrays.toString(arr));
        long start = System.currentTimeMillis();
        sort.sort(arr);
        long end = System.currentTimeMillis();
        System.out.println("After: " + Arrays.toString(arr));
        System.out.println("Runtime: " + (end - start) + " ms\n");

    }

}
