package sort.impl;

import sort.Sort;

public class BubbleSort extends Sort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (arr[j] > arr[j + 1]) swap(arr, j, j + 1);
    }

    @Override
    public String getAlgorithm() {
        return "BUBBLE SORT";
    }
}
