package sort.impl;

import sort.Sort;

public class QuickSort extends Sort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        quickSort(arr, 0, n - 1);
    }

    private void quickSort(int[] arr, int l, int h) {
        if (l < h) {
            int pivot = partition(arr, l, h);
            quickSort(arr, l, pivot - 1);
            quickSort(arr, pivot + 1, h);
        }
    }

    protected int partition(int[] arr, int l, int h) {
        int pivot = arr[h];
        int x = l;
        for (int i = l; i < h; i++) {
            if (arr[i] < pivot) {
                swap(arr, i, x);
                x++;
            }
        }
        swap(arr, x, h);
        return x;
    }

    @Override
    public String getAlgorithm() {
        return "QUICK SORT";
    }
}
