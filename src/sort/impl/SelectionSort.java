package sort.impl;

import sort.Sort;

public class SelectionSort extends Sort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        for (int i = 0; i < n; i++)
            for (int j = i + 1; j < n; j++)
                if (arr[j] < arr[i]) swap(arr, i, j);
    }

    @Override
    public String getAlgorithm() {
        return "SELECTION SORT";
    }
}
