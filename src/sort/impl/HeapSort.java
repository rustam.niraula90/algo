package sort.impl;

import sort.Sort;

public class HeapSort extends Sort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        buildHeap(arr);
        for (int i = n - 1; i > 0; i--) {
            swap(arr, 0, i);
            heapify(arr, 0, i);
        }
    }

    private void heapify(int[] arr, int i, int h) {
        int l = i * 2 + 1;
        int r = i * 2 + 2;
        int max;
        if (l < h && arr[l] > arr[i])
            max = l;
        else max = i;
        if (r < h && arr[r] > arr[max])
            max = r;
        if (i != max) {
            swap(arr, i, max);
            heapify(arr, max, h);
        }
    }

    private void buildHeap(int[] arr) {
        for (int i = arr.length / 2 - 1; i >= 0; i--)
            heapify(arr, i, arr.length - 1);
    }

    @Override
    public String getAlgorithm() {
        return "HEAP SORT";
    }
}
