package sort.impl;

import sort.Sort;

public class InsertionSort extends Sort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        for (int i = 1; i < n; i++) {
            int x = arr[i];
            int j = i - 1;
            while (j >= 0 && arr[j] > x) {
                swap(arr, j, j + 1);
                j--;
            }
            arr[j + 1] = x;
        }
    }

    @Override
    public String getAlgorithm() {
        return "Insertion SORT";
    }
}
