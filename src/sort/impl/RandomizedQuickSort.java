package sort.impl;

import java.util.Random;

public class RandomizedQuickSort extends QuickSort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        quickSort(arr, 0, n - 1);
    }

    private void quickSort(int[] arr, int l, int h) {
        if (l < h) {
            int pivot = randomPartition(arr, l, h);
            quickSort(arr, l, pivot - 1);
            quickSort(arr, pivot + 1, h);
        }
    }

    private int randomPartition(int[] arr, int l, int h) {
        int r = new Random().nextInt(l, h + 1);
        swap(arr, r, h);
        return partition(arr, l, h);
    }

    @Override
    public String getAlgorithm() {
        return "RANDOMIZED QUICK SORT";
    }
}
