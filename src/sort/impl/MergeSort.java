package sort.impl;

import sort.Sort;

public class MergeSort extends Sort {
    @Override
    public void sort(int[] arr) {
        int n = arr.length;
        mergeSort(arr, 0, n - 1);
    }

    private void mergeSort(int[] arr, int l, int h) {
        if (l < h) {
            int m = (int) Math.floor((l + h) / 2.0);
            mergeSort(arr, l, m);
            mergeSort(arr, m + 1, h);
            merge(arr, l, m + 1, h);
        }
    }

    private void merge(int[] arr, int l, int m, int h) {
        int[] temp = new int[h + 1];
        int x = l;
        int y = m;
        int z = l;
        while (x < m && y <= h) {
            if (arr[x] < arr[y]) {
                temp[z] = arr[x];
                x++;
            } else {
                temp[z] = arr[y];
                y++;
            }
            z++;
        }
        while (x < m) {
            temp[z] = arr[x];
            x++;
            z++;
        }
        while (y <= h) {
            temp[z] = arr[y];
            y++;
            z++;
        }
        for (int i = l; i <= h; i++) {
            arr[i] = temp[i];
            swap(arr, i, i);
        }
    }

    @Override
    public String getAlgorithm() {
        return "MERGE SORT";
    }
}
