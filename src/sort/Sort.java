package sort;

public abstract class Sort {

    abstract public void sort(int[] arr);

    protected void swap(int[] arr, int index1, int index2) {
        int t = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = t;
    }

    abstract public String getAlgorithm();
}
