package searching;

public abstract class Search<T, E> {

    public abstract T search(int arr[], E elem);

    public abstract String getAlgorithm();
}
