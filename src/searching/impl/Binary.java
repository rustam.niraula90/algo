package searching.impl;

import searching.Search;

public class Binary extends Search<Integer, Integer> {
    @Override
    public Integer search(int[] arr, Integer elem) {
        int l = 0;
        int h = arr.length - 1;
        while (l <= h) {
            int m = (int) Math.floor((l + h) / 2.0);
            if (arr[m] == elem) return m;
            if (elem < arr[m])
                h = m - 1;
            else l = m + 1;
        }

        return -1;
    }

    @Override
    public String getAlgorithm() {
        return "BINARY SEARCH";
    }
}
