package searching.impl;

import searching.Search;

public class MinMaxFinding extends Search<MinMaxFinding.MinMax, Void> {

    @Override
    public MinMax search(int[] arr, Void elem) {
        MinMax minMax = new MinMax();
        findMinMax(arr, 0, arr.length - 1, minMax);
        return minMax;
    }

    public void findMinMax(int[] arr, int l, int h, MinMax minMax) {
        if (l == h)
            minMax.set(arr[l], arr[l]);
        else if (l == h - 1)
            if (arr[l] < arr[h])
                minMax.set(arr[l], arr[h]);
            else minMax.set(arr[h], arr[l]);
        else {
            int m = (int) Math.floor((l + h) / 2.0);
            findMinMax(arr, l, m, minMax);
            MinMax minMax1 = new MinMax();
            findMinMax(arr, m + 1, h, minMax1);
            if (minMax1.min < minMax.min)
                minMax.min = minMax1.min;
            if (minMax1.max > minMax.max)
                minMax.max = minMax1.max;
        }
    }

    @Override
    public String getAlgorithm() {
        return "MIN-MAX FINDING";
    }

    public static class MinMax {
        int min;
        int max;

        public MinMax() {
        }

        public void set(int min, int max) {
            this.min = min;
            this.max = max;
        }

        @Override
        public String toString() {
            try {
                return "min: " + min
                        + "\nmax: " + max;
            } catch (Exception e) {
                return "N/A";
            }
        }
    }
}
