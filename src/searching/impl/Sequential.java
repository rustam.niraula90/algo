package searching.impl;

import searching.Search;

public class Sequential extends Search<Integer, Integer> {

    @Override
    public Integer search(int[] arr, Integer e) {
        for (int j = 0; j < arr.length; j++)
            if (arr[j] == e) return j;
        return -1;
    }

    @Override
    public String getAlgorithm() {
        return "SEQUENTIAL SEARCH";
    }
}
