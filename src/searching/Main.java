package searching;

import searching.impl.Binary;
import searching.impl.MinMaxFinding;
import searching.impl.Sequential;
import util.ArrayUtil;

import java.util.Random;

public class Main {

    public static void main(String[] args) {
        final int array_size = 100000;
        int[] arr = ArrayUtil.createSortedArray(array_size);
        int elem = new Random().nextInt(array_size);
        search(arr, arr[elem], new Binary());

        arr = ArrayUtil.shuffle(arr);
        search(arr, arr[elem], new Sequential());

        search(arr, null, new MinMaxFinding());
    }

    private static <T, E> T search(int[] arr, E elem, Search<T, E> search) {
        System.out.println(search.getAlgorithm());
        long start = System.currentTimeMillis();
        T index = search.search(arr, elem);
        long end = System.currentTimeMillis();
        if (index instanceof Integer i) {
            System.out.println("index: " + index + " element: " + (i != -1 ? arr[i] : "N/A"));
        } else {
            System.out.println(index);
        }
        System.out.println("Runtime: " + (end - start) + " ms\n");
        return index;
    }
}
